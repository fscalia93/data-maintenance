import ConfigLoader, { ExcelTableEntry } from "../config/ConfigLoader"
import { stringSplitter } from "../utils/stringSplitter"

export type  EmailInput = {
  name: string,
  email: string,
  htmlTables: {
    html: string,
    name: string,
    config: ExcelTableEntry
  }[]
}

export type EmailReturn = {
  toEmail: string,
  subject: string,
  body: string,
  attachment: string
}

export const createEmail = (input: EmailInput): EmailReturn => {
  const globalConfig = ConfigLoader.getInstance().getConfig()

  let body = `<div> <p>Hello ${input.name}, </p> \n \n <br/> <br/>
  <p> ${globalConfig.onMail?.salutation} \n \n <br/> <br/>` 

  for(const table of input.htmlTables){

    body = body + `${stringSplitter(table.config.title)}: \n\n ${table.html} \n\n`
  }

  body = body + globalConfig.onMail?.closing

  return {
    toEmail: input.email,
    subject: globalConfig.onMail?.subject || 'subject',
    body: body,
    attachment: ''
  }
}

export const parseDataToEmailOutput = (inputs: {[index: string]: EmailInput}) =>{
  const output = []

  for(const key of Object.keys(inputs)){

    output.push(createEmail(inputs[key]))
  }

 
  return output
  
}