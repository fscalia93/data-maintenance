import { stringSplitter } from "../utils/stringSplitter";
import { DataTypeconfig } from "..";
import { Config } from "../config/ConfigLoader";

export const createTable = (data: any[], config: DataTypeconfig , commonConfig: Config) => {
  const headers = Object.keys(data[0]).filter(x => x !== config.configData.email && !commonConfig.excludeColumnsInMail.includes(x))

  let html = '<table class="table table-striped">';
    html += '<tr>';
    
    for(const header of headers){
      html += '<th>'+stringSplitter(header)+'</th>';
    }

    html += '</tr>';

    for(const date of data){
      html += '<tr>';
      const values = Object.entries(date)
      for(const entry of values){
        const key =  entry[0]
        const val = entry[1]

        if(headers.includes(key)){
          html += '<td>'+val+'</td>';
        }
      }
   
      html += '<tr>';

    }
    
     html += '</table>';
     return html
}