import * as Excel from "exceljs";
import ConfigLoader, { ExcelTableEntry } from "../config/ConfigLoader";
import { nextChar } from "../utils/letterIncreaser";
import { sortById } from "../utils/sortById";

export const readExcelFile = async (path: string) => {
  const workbook = new Excel.Workbook();
  const res = await workbook.xlsx.readFile(path)
  return res
  
};

export const showcontent = async(worksheet: Excel.Worksheet) => {

  worksheet.eachRow(row => {
    row.eachCell(cell => {
      console.log(cell.value)
    })
  });
}

export const readWorksheetTables = async(worksheet: Excel.Worksheet) => {
  const tables =  worksheet.getTables()

  for(const table of tables){
    console.log(table)
    const myTable = table["table"] as Excel.Table
    console.log(myTable.rows)
   
  }
}

const getCellValue =(cell: Excel.Cell): string => {

  let cellValue = cell.value

  if(cellValue instanceof Object){
    cellValue  = (cellValue as any).result
  }

  if(cellValue instanceof Object){
    if (cellValue["error"]){
      cellValue = null
    } 
  }

  const entry : string = cellValue as string
  return entry

}

export const parseExcelSheetTable = async(worksheet: Excel.Worksheet, configTable: ExcelTableEntry) => {
    const globalConfig = ConfigLoader.getInstance().getConfig()

    const table =  worksheet.getTable(configTable.tableName)
    const myTable = table["table"] as Excel.Table
    const tableSize = (myTable as any).tableRef as string
    const cellSizes = (tableSize.match(/[a-zA-Z]+|[0-9]+/g))

    const config = {
      startColumn: cellSizes[0],
      endColumn: cellSizes[2],
      startRow: Number(cellSizes[1]),
      endRow: Number(cellSizes[3]),
      columnNames: [],
      name: configTable.tableName,
      configData: configTable
    }

    const data = [ ]

  
    let actualCol = config.startColumn

    while(true){

      const rowId = config.startRow
      const row = worksheet.getRow(rowId)
      const cell = row.getCell(actualCol).value

      config[actualCol] = {
        name: cell
      }
      config.columnNames.push(actualCol)
      if(actualCol === config.endColumn) break
      actualCol = nextChar(actualCol)
    }
 
  

    console.log(config)

    for(let rowId = config.startRow+1; rowId <= config.endRow; rowId++){
     
      let myData = {}
      let isEmptyRow = true

      for(const column of config.columnNames){
        const row = worksheet.getRow(rowId)
        const entry = getCellValue(row.getCell(column))
        if(entry) isEmptyRow = false
      }

      if(isEmptyRow) continue
      for(const column of config.columnNames){
        const row = worksheet.getRow(rowId)

      const entry = getCellValue(row.getCell(column))

      if(config[column].name === configTable.id){

        if(configTable.excludeIds && configTable.excludeIds.includes(entry)){
          myData = null;
          break;
        }
        if (configTable.specificIds && !configTable.specificIds.includes(entry)){
          myData = null;
          break;
        }

      }


        if(
          (!globalConfig.skipEntries || !globalConfig.skipEntries.map(x => x.toLowerCase()).includes(entry ? entry.toLowerCase() : entry)) &&
          (!configTable.skipEntries || !configTable.skipEntries.map(x => x.toLowerCase()).includes(entry ? entry.toLowerCase() : entry))
          ){
          
            myData[config[column].name] = entry
        }
        else {
          myData = null;
          break;
        }
      }
      myData && data.push(myData)
     
    }

  
    const sortedIds = sortById(data, configTable.id, configTable)

    return {config, data, sortedIds}
}


export const readTable = async(worksheet: Excel.Worksheet, tableName: string) => {

  const table = worksheet.getTable(tableName);

  console.log(table.displayName)
  console.log(table.headerRow)
  console.log(table.name)
  console.log(table.totalsRow)
  const data = table.rows

  for(const row of data){
    console.log(row)
  }

}
