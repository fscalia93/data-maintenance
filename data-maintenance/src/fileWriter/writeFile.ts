import * as fs from 'fs'
import * as path from 'path'
export const saveFile = async(content: any, pathToStore: string) => {
  const dir = path.dirname(pathToStore)

  await fs.promises.mkdir(dir, { recursive: true })
  await fs.writeFileSync(pathToStore, JSON.stringify(content))
}