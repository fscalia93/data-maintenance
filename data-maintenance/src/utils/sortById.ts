import ConfigLoader, { ExcelTableEntry } from "../config/ConfigLoader"


export const sortById = (table: any[], idColumn: string, configTable: ExcelTableEntry) => {
  const sortedList = {}
  const config = ConfigLoader.getInstance().getConfig()

  const ids = table.map(x => x[idColumn])
  let uniqueIds: string[] = Array.from(new Set(ids))

  if(config.specificIds){
    uniqueIds = uniqueIds.filter(x => config.specificIds.includes(x))

  }
  else if(config.excludeIds){
    uniqueIds = uniqueIds.filter(x => !config.excludeIds.includes(x))

  }

  sortedList["error"] = []  

  
  for(const id of uniqueIds){
    if(id === null){
      sortedList["error"] =  sortedList["error"].concat(table.filter(x => x[idColumn] === id))
     
    }
    else{
      const filteredTable =  table.filter(x => x[idColumn] === id)
      
      const filteredErrorValues = filteredTable.filter(x => !x[configTable.email] || !x[configTable.name])
      const others = filteredTable.filter(x => !filteredErrorValues.includes(x))

      sortedList["error"] =  sortedList["error"].concat(filteredErrorValues)

      if(others.length > 0){
        sortedList[id] = others
      }
      
    }
    
  }

  if(sortedList["error"].length === 0){
    delete sortedList["error"]
  }

  return sortedList
}

export const getUniqueIds = (keys: string[]) => {
  return Array.from(new Set(keys))  
}