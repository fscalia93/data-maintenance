import * as fs from 'fs'

export type ExcelTableEntry = {
  "worksheet": string,
  "tableName": string,
  "id": string,
  "name":  string,
  "email": string,
  title: string,
  skipEntries?: string[],
  specificIds?: string[],
  excludeIds?: string[], 
}

export type Config = {
  excelPath: string,
  excludeColumnsInMail: string[],
  excelData: ExcelTableEntry[],
  specificIds?: string[],
  excludeIds?: string[], 
  skipEntries?: string[],
  onError: {
    id: number,
    to: string
  }
  onMail?: {
    salutation: string,
    closing : string,
    subject: string
  }
}

export default class ConfigLoader {

  config: Config
  static instance: ConfigLoader;

  constructor(path: string){
    const rawData = fs.readFileSync(path, {encoding: "utf-8"})
    const config = JSON.parse(rawData)
    this.config = config
    instance = config
  }

  static getInstance(options?: string) {
    if (!ConfigLoader.instance) {
      ConfigLoader.instance = new ConfigLoader(options);
    }
    return ConfigLoader.instance;
  }

  getConfig(){
    return this.config
  }
  getExcelPath(){
    return this.config.excelPath
  }
}

let instance;
module.exports == {
  getInstance(path: string){
    if(!instance){
      instance = new ConfigLoader(path)
    }
    return instance
  }
}