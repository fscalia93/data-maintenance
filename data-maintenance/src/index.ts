import { Workbook } from "exceljs";
import ConfigLoader, { Config, ExcelTableEntry } from "./config/ConfigLoader";
import { EmailInput, parseDataToEmailOutput } from "./emailParser/createEmail";
import { saveFile } from "./fileWriter/writeFile";
import { parseExcelSheetTable, readExcelFile } from "./parser/excelParser";
import { createTable } from "./table/createHTMLTable";
import { getUniqueIds} from "./utils/sortById";

console.log("i'm working");

export type DataTypeconfig = {
  startColumn: string;
    endColumn: string;
    startRow: number;
    endRow: number;
    columnNames: any[],
    name: string,
    configData: ExcelTableEntry
}


type DataType = {
  config: DataTypeconfig;
  data: any[];
  sortedIds: any
}

let commonConfig: Config = null

const createEmailOutputv2 = (tables: DataType[], config: Config) => {

  const emailInputList: {[index: string]: EmailInput}  = {}
  let errorData = []
  let ids = []
  let isError = false
  for(const table of tables){
    ids = ids.concat(Object.keys(table.sortedIds))
  }

  const uniqueIds = getUniqueIds(ids)

  for(const id of uniqueIds){


    const data = {
      name: '',
      email:  '',
      htmlTables: [],
    }
    

    for(const table of tables){

    

      if(table.sortedIds[id]){
        const htmlTable = createTable(table.sortedIds[id], table.config,commonConfig)
        data.name = table.sortedIds[id][0][table.config.configData.name]
        data.email = table.sortedIds[id][0][table.config.configData.email]
        data.htmlTables.push({html:htmlTable, config: table.config.configData, name: table.config.name}) 
      }
      
    }

    if(id === "error" || !data.name || !data.email){
      console.log ("Error no Name or Email found")
      isError = true
      emailInputList[config.onError.id] = {
        name: data.name,
        email: config.onError.to,
        htmlTables: data.htmlTables
      }
      errorData = errorData.concat(data.htmlTables)
    }
    else {
      emailInputList[id] = data
    }
   
  }

  return ({isError, errorData, data: parseDataToEmailOutput(emailInputList)})

}




const parseExcelTable = async(workBook: Workbook, config: ExcelTableEntry) => {
  const pmWorksheet = workBook.getWorksheet(config.worksheet)
  const tableData = await parseExcelSheetTable(pmWorksheet, config)
  return tableData
}




const entry = async() => {
  const config = ConfigLoader.getInstance("data/conf.json")
  commonConfig = config.getConfig()

  const excelFile= await readExcelFile(config.getExcelPath())
 
  

  const tableData: DataType[] = []

  const configData = commonConfig.excelData
  for(const configDate of configData){
    tableData.push( await parseExcelTable(excelFile, configDate))
  }

  console.log(tableData)
 
  const emailOutput = createEmailOutputv2(tableData, commonConfig)
  await saveFile(emailOutput, "output/toMail.json")
  console.log("ToEmail file stored successfull")

}

entry()